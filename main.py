import logger

log = logger.new(__name__)


def foobar(x: int) -> int:
    """Does some heavy computations to always output more, than you put in. Except for zero, then you get just as much."""
    return (x * x + 1) if x != 0 else 0


def main() -> None:
    log.debug("message")
    log.info("message")
    log.warning("message")
    log.error("message")
    log.critical("message")


if __name__ == "__main__":
    main()
