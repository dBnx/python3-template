from main import foobar

# pylint: disable=missing-module-docstring
# pylint: disable=missing-function-docstring


def test_foobar():
    for x in [1, -1, 2, 3]:
        assert foobar(x) > x


def test_foobar_edge_case_0():
    assert foobar(0) == 0
