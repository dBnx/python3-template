"""
Provides a simple function to instantiate a pretty logger.

Most of the code was adapted from: https://stackoverflow.com/a/56944256
"""
import logging

__all__ = ["new"]


class CustomFormatter(logging.Formatter):

    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format_str: str = "%(asctime)s.%(msecs)03d %(levelname)-8s %(name)s: %(message)s (%(filename)s:%(lineno)d)"  # noqa
    datefmt = "%Y-%m-%d %H:%M:%S"

    FORMATS = {
        logging.DEBUG: grey + format_str + reset,
        logging.INFO: grey + format_str + reset,
        logging.WARNING: yellow + format_str + reset,
        logging.ERROR: red + format_str + reset,
        logging.CRITICAL: bold_red + format_str + reset,
    }

    def format(self, record):
        fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(fmt=fmt, datefmt=self.datefmt)
        return formatter.format(record)


def new(name: str, level=logging.DEBUG) -> logging.Logger:
    """
    Usage: `log = logger.new(__name__)` to use the module name or the app name
    in the main entry point.
    """
    logger = logging.getLogger(name)
    logger.setLevel(level)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(level)

    ch.setFormatter(CustomFormatter())

    logger.addHandler(ch)
    return logger
